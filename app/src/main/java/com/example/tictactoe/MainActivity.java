package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private String [] board = new String[9];
    private int[][] strikes = new int [][]{{0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6}};
    private ArrayList<View> views = new ArrayList<>();
    private String currentTurn = "x";
    private int countOfTurns = 0;
    private int markNumber = 0;
    private ImageView currentTurnImage;
    private ImageView markImage;
    private Button playAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentTurnImage = (ImageView) findViewById(R.id.currentTurn);
        playAgain = (Button) findViewById(R.id.playagain);
        markImage = (ImageView) findViewById(R.id.mark);
        setUpListeners();
        initBoard();
    };

    private void initBoard(){
        Arrays.fill(board, "e");
    }

    private boolean checkForWin(){
        for (int strike=0; strike <  strikes.length; strike++){
            int count = 0;
            for (int positionInStrike: strikes[strike]){
                if (board[positionInStrike].equals(currentTurn)){
                    count++;
                    if (count == 3){
                        markNumber = strike + 1;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void setUpListeners(){
        ImageView p0 = (ImageView) findViewById(R.id.p0);
        ImageView p1 = (ImageView) findViewById(R.id.p1);
        ImageView p2 = (ImageView) findViewById(R.id.p2);
        ImageView p3 = (ImageView) findViewById(R.id.p3);
        ImageView p4 = (ImageView) findViewById(R.id.p4);
        ImageView p5 = (ImageView) findViewById(R.id.p5);
        ImageView p6 = (ImageView) findViewById(R.id.p6);
        ImageView p7 = (ImageView) findViewById(R.id.p7);
        ImageView p8 = (ImageView) findViewById(R.id.p8);
        views.add(p0);views.add(p1);views.add(p2);views.add(p3);views.add(p4);views.add(p5);
        views.add(p6);views.add(p7);views.add(p8);

        for (View v: views){v.setOnClickListener(this);}

        playAgain.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                playAgain();
            }
        });
    }

    private int getXorOResource(){
        return currentTurn.equals("x")? R.drawable.x: R.drawable.o;
    }

    private int getCurrentResource(){
        return currentTurn.equals("x")? R.drawable.xplay: R.drawable.oplay;
    }

    private int getWinnerResource(){
        return currentTurn.equals("x")? R.drawable.xwin: R.drawable.owin;

    }

    private int getMarkResourceByNumber(){
        switch (markNumber){
            case(1): return R.drawable.mark1;
            case (2): return R.drawable.mark2;
            case (3): return R.drawable.mark3;
            case (4): return R.drawable.mark4;
            case (5): return R.drawable.mark5;
            case (6): return R.drawable.mark6;
            case (7): return R.drawable.mark7;
            case (8): return R.drawable.mark8;
        }
        return 0;
    }

    private void makeTurn(View v, int position){
        countOfTurns++;
        v.setClickable(false);
        v.setBackgroundResource(getXorOResource());
        board[position] = currentTurn;
        boolean win = checkForWin();
        if (win || countOfTurns == 9){
            stopGame(win);
        }
        else{
            currentTurn = currentTurn.equals("x") ?  "o":  "x";
            currentTurnImage.setImageDrawable(ContextCompat.getDrawable(this, getCurrentResource()));
        }
    }

    private void stopGame(boolean win){
        if (countOfTurns ==9 && !win){
            currentTurnImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nowin));
        }
        else
        {
            currentTurnImage.setImageDrawable(ContextCompat.getDrawable(this, getWinnerResource()));
            markImage.setImageDrawable(ContextCompat.getDrawable(this, getMarkResourceByNumber()));
            markImage.bringToFront();
            for (View v:views){v.setClickable(false);}
        }
        playAgain.setVisibility(View.VISIBLE);

    }

    private void playAgain(){
        initBoard();
        markImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.empty));
        currentTurnImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.xplay));
        playAgain.setVisibility(View.INVISIBLE);
        countOfTurns = 0;
        currentTurn = "x";
        for (View v:views){
            v.setBackgroundResource(R.drawable.empty);
            v.setClickable(true);
        }
    }



    @Override
    public void onClick(View v) {
        String position = (String) v.getTag();
        int intPosition = position.charAt(0) - '0';
        makeTurn(v, intPosition);
    }
}